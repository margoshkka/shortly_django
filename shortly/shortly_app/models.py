from django.db import models

# Create your models here.


class ShortedUrl(models.Model):
    full_url =  models.URLField(unique=True)
    shorted_url = models.IntegerField(unique=True)
    visit_counter = models.IntegerField(default=0)