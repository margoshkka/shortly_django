from django.shortcuts import render, redirect
from django.contrib import messages
from .models import ShortedUrl

# Create your views here.
import random


def index(request):
    return redirect('/shortly')


def all_urls(request):
    urls = ShortedUrl.objects.order_by('-visit_counter')

    context = {
        'urls': urls
    }
    return render(request,'shortly_app/index.html', context)


def top_urls(request):
    urls = ShortedUrl.objects.order_by('-visit_counter')[:10]

    context = {
        'urls': urls
    }
    return render(request, 'shortly_app/index.html', context)


def add_url(request):
    if request.method == 'POST':
        new_url = request.POST.get('url')
        find_if_added = ShortedUrl.objects.filter(full_url=new_url)
        if new_url is '':
            messages.warning(request, 'An empty link field!')
        elif find_if_added.first() is None:
            task = ShortedUrl(full_url=new_url,
                          shorted_url=generate_shortly())
            task.save()
            messages.success(request,  'This link has been added successfully!')
        else:
            messages.warning(request, 'This link is already in a list!')

    return redirect('/shortly')


def generate_shortly():
    shortly = random.randint(0, 999999)
    while ShortedUrl.objects.filter(shorted_url=shortly).first() is not None:
        shortly = random.randint(0, 999999)
    return shortly


def follow_url(request, sh_url):
    try:
        shorted_url = ShortedUrl.objects.filter(shorted_url=sh_url).first()
        shorted_url.visit_counter += 1
        shorted_url.save()
        return redirect(shorted_url.full_url)

    except ShortedUrl.DoesNotExist as e:
        print(e.message)
    except AttributeError as e:
        print(e.message)
